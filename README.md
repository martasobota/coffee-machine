# coffee-machine

Coffee machine runs with python3.

If you would like to prepare yourself a coffee:
`python3 main.py` will prepare espresso, cappuccino and americano for you and 
your friends.

If you would like to add another type of coffee, add it to `COFFEE_MENU `
dictionary in `coffee_machine/menu.py`. 

All you need is to add coffee's name
as the key (e.g. `latte`) and assign to it tuple with `water, coffee beans, milk`
values.
* `water` - amount of water needed to prepare coffee [ml]
* `coffee beans` - weight of coffee beans required [gr]
* `milk` - how much milk is required [ml]

For example you can add `"latte": (200, 25, 100)` so our new menu will
look as follows:

```
COFFEE_MENU = {
    "espresso": (150, 15, 0,),
    "americano": (200, 25, 0),
    "cappuccino": (150, 20, 100),
    "latte": (200, 25, 100)
}
```

To run tests you will need install `pip install -r requirements.txt`, where
`pytets` library is added.
Run tests with `pytest tests/`.

Enjoy! ☕️