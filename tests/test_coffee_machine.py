"""Tests for Coffee Machine"""

import pytest

from coffee_machine.machinery import CoffeeMachine
from coffee_machine.exceptions import (
    WaterLevelTooLow, NotEnoughCoffeeBeans, CoffeeGroundsTankFull,
    MilkLevelTooLow, TemperatureError, MachineError
)


def test_coffee_machine_starts_with_proper_values():
    cm = CoffeeMachine()
    assert cm.water_level == 0
    assert cm.coffee_beans == 0
    assert cm.milk == 0
    assert cm.coffee_grounds == 0
    assert cm.temperature == 20
    assert cm.coffee_counter == 0

def test_refill_water_adds_proper_value_to_water_level():
    cm = CoffeeMachine()
    assert cm.water_level == 0
    cm.refill_water(500)
    assert cm.water_level == 500
    cm.refill_water(1000)
    assert cm.water_level == 1500

def test_refill_coffee_adds_proper_value_to_coffee_beans():
    cm = CoffeeMachine()
    assert cm.coffee_beans == 0
    cm.refill_coffee(1000)
    assert cm.coffee_beans == 1000
    cm.refill_coffee(2500)
    assert cm.coffee_beans == 3500

def test_refill_milk_adds_proper_value_to_milk():
    cm = CoffeeMachine()
    assert cm.milk == 0
    cm.refill_milk(750)
    assert cm.milk == 750
    cm.refill_milk(250)
    assert cm.milk == 1000

def test_heat_up_raises_temperature_to_95():
    cm = CoffeeMachine()
    assert cm.temperature == 20
    cm.heat_up()
    assert cm.temperature == 95

def test_check_raises_when_water_level_is_below_200():
    cm = CoffeeMachine()
    cm.refill_water(50)
    with pytest.raises(WaterLevelTooLow):
        cm.check()

def test_check_raises_when_coffee_beans_below_50():
    cm = CoffeeMachine()
    cm.water_level = 200
    cm.refill_coffee(30)
    with pytest.raises(NotEnoughCoffeeBeans):
        cm.check()

def test_check_raises_when_milk_level_below_100():
    cm = CoffeeMachine(200, 100, milk=50)
    with pytest.raises(MilkLevelTooLow):
        cm.check()

def test_check_raises_when_coffee_grounds_above_500():
    cm = CoffeeMachine(200, 150, 200, coffee_grounds=600)
    with pytest.raises(CoffeeGroundsTankFull):
        cm.check()

def test_check_raises_when_temperature_below_10():
    cm = CoffeeMachine(200, 50, 200, coffee_grounds=500)
    cm.temperature = 5
    with pytest.raises(TemperatureError):
        cm.check()

def test_check_raises_when_temperature_equals_98():
    cm = CoffeeMachine(200, 50, 200, 500)
    cm.temperature = 98
    with pytest.raises(TemperatureError):
        cm.check()

def test_check_raises_when_temperature_above_98():
    cm = CoffeeMachine(200, 50, 200, 500)
    cm.temperature = 99
    with pytest.raises(TemperatureError):
        cm.check()

def test_check_raises_when_coffee_counter_above_20k():
    cm = CoffeeMachine(200, 50, 200, 400)
    cm.coffee_counter = 20001
    cm.temperature = 90
    with pytest.raises(MachineError):
        cm.check()

def test_update_state_changes_values_properly():
    cm = CoffeeMachine(750, 750, 750, 750)
    cm.temperature = 95
    cm.update_state(150, 50, 50)
    assert cm.water_level == 600
    assert cm.coffee_beans == 700
    assert cm.milk == 700
    assert cm.coffee_grounds == 805
    assert cm.temperature == 95
    assert cm.coffee_counter == 1

def test_prepare_coffee_makes_good_espresso():
    cm = CoffeeMachine(750, 750, 750, 300)
    cm.temperature = 80
    cm.prepare_coffee("espresso")
    assert cm.water_level == 600
    assert cm.coffee_beans == 735
    assert cm.milk == 750
    assert cm.coffee_grounds == 320
    assert cm.temperature == 95
    assert cm.coffee_counter == 1


def test_prepare_coffee_makes_good_americano():
    cm = CoffeeMachine(750, 750, 750, 300)
    cm.temperature = 80
    cm.prepare_coffee("americano")
    assert cm.water_level == 550
    assert cm.coffee_beans == 725
    assert cm.milk == 750
    assert cm.coffee_grounds == 330
    assert cm.temperature == 95
    assert cm.coffee_counter == 1


def test_prepare_coffee_makes_good_cappuccino():
    cm = CoffeeMachine(750, 750, 750, 300)
    cm.temperature = 80
    cm.prepare_coffee("cappuccino")
    assert cm.water_level == 600
    assert cm.coffee_beans == 730
    assert cm.milk == 650
    assert cm.coffee_grounds == 325
    assert cm.temperature == 95
    assert cm.coffee_counter == 1

def test_coffee_counter_counts_properly():
    cm = CoffeeMachine(1500, 750, 750, 0)
    cm.temperature = 80
    cm.prepare_coffee("espresso")
    cm.prepare_coffee("espresso")
    cm.prepare_coffee("americano")
    cm.prepare_coffee("cappuccino")
    cm.prepare_coffee("cappuccino")
    assert cm.coffee_counter == 5

def test_check_not_raises_with_proper_values():
    cm = CoffeeMachine(200, 50, 200, 500)
    cm.temperature = 90
    cm.coffee_counter = 20000

