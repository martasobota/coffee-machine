"""Coffee machine's menu.
Name of the coffee is the key and its values are:
water, coffee beans, milk, grounds stored in the tuple"""

COFFEE_MENU = {
    "espresso": (150, 15, 0),
    "americano": (200, 25, 0),
    "cappuccino": (150, 20, 100)
}
