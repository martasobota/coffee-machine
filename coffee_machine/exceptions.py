"""Exceptions for Coffee Machine"""

class WaterLevelTooLow(Exception):
    """Raised when water level is below 200ml"""
    def __init__(self, message='Water level too low - fill the tank'):
        super(WaterLevelTooLow, self).__init__(message)
        self.message = message

class NotEnoughCoffeeBeans(Exception):
    """Raised when there is less than 50gr of coffee grounds in the container"""
    def __init__(self, message='Coffee beans container is empty'):
        super(NotEnoughCoffeeBeans, self).__init__(message)
        self.message = message

class CoffeeGroundsTankFull(Exception):
    """Raised when coffee grounds tank contains more than 500gr"""
    def __init__(self, message='Coffee grounds tank is full - empty it'):
        super(CoffeeGroundsTankFull, self).__init__(message)
        self.message = message

class MilkLevelTooLow(Exception):
    """Raised when milk container is below 100mlr"""
    def __init__(self, message='Milk level too low - fill the milk container'):
        super(MilkLevelTooLow, self).__init__(message)
        self.message = message

class TemperatureError(Exception):
    """Raised when temperature is <10°C or >=98°Cr"""
    def __init__(self, temp, message='Temperature out off allowed levels'):
        super(TemperatureError, self).__init__(message)
        self.message = f"{message}: {temp}"

class MachineError(Exception):
    """Raised when coffee_counter exceeds 20k and user should buy new machine"""
    def __init__(self, message='Very serious error'):
        super(MachineError, self).__init__(message)
        self.message = message
