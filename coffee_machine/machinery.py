"""Coffee Machine Machinery"""

import time

from coffee_machine.menu import COFFEE_MENU
from coffee_machine.exceptions import (
    WaterLevelTooLow, NotEnoughCoffeeBeans, CoffeeGroundsTankFull,
    MilkLevelTooLow, TemperatureError, MachineError
)


class CoffeeMachine():
    """Coffee Machine"""

    def __init__(self,
                 water_level=0,
                 coffee_beans=0,
                 milk=0,
                 coffee_grounds=0):
        self.water_level = water_level
        self.coffee_beans = coffee_beans
        self.milk = milk
        self.coffee_grounds = coffee_grounds
        self.temperature = self.measure_temperature()
        self.coffee_counter = 0

    @staticmethod
    def timer(seconds): # add thread or game loop for stand_by state
        """Draft version of timer for future stand by"""
        count = seconds
        while count > 0:
            time.sleep(1)
            count -= 1
        print("Counting is over")

    def refill_water(self, milliliters):
        """Adds water to self.water_level in ml"""
        self.water_level += milliliters

    def refill_coffee(self, grams):
        """Adds water to self.coffee_beans in gr"""
        self.coffee_beans += grams

    def refill_milk(self, milliliters):
        """Adds milk to self.milk in ml"""
        self.milk += milliliters

    @staticmethod
    def measure_temperature():
        """Measure temperature of flow heater"""
        return 20

    def heat_up(self):
        """Heats flow heater"""
        print("Heating up...")
        # time.sleep(1)
        self.temperature = 95

    def __repr__(self):
        return str(self.__dict__)

    def __str__(self):
        return (
            f"Water level: {self.water_level}ml\n"
            f"Coffee beans: {self.coffee_beans}gr\n"
            f"Milk: {self.milk}ml\n"
            f"Coffee grounds: {self.coffee_grounds}gr\n"
            f"Temp: {self.temperature}°C\n"
            f"Coffee counter: {self.coffee_counter}"
        )

    def check(self):
        """Checks if machine is ready to prepare coffee"""
        if self.water_level < 200:
            raise WaterLevelTooLow
        if self.coffee_beans < 50:
            raise NotEnoughCoffeeBeans
        if self.milk < 100:
            raise MilkLevelTooLow
        if self.coffee_grounds > 500:
            raise CoffeeGroundsTankFull
        if (self.temperature < 10 or self.temperature >= 98):
            raise TemperatureError(self.temperature)
        if self.coffee_counter > 20000:
            raise MachineError

    def update_state(self, water, beans, milk):
        """Updates machine's state depending on prepared coffee"""
        self.water_level -= water
        self.coffee_beans -= beans
        self.milk -= milk
        self.coffee_grounds += (beans+5)
        self.temperature -= 15
        self.coffee_counter += 1
        self.heat_up()

    def prepare_coffee(self, coffee_type):
        """Prepares coffee according to given coffee type.
        Gets coffee's parameters from COFFEE_MENU dictionary, where
        key is coffee type, value it's properties stored in the tuple"""
        try:
            self.check()
            self.update_state(*COFFEE_MENU.get(coffee_type))
            print(f"{coffee_type} is ready!")
        except (
                WaterLevelTooLow, NotEnoughCoffeeBeans,
                CoffeeGroundsTankFull, MilkLevelTooLow, TemperatureError
        ) as exc:
            print("Error: ", exc.message)
