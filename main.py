from coffee_machine.machinery import CoffeeMachine

if __name__ == '__main__':
    cm = CoffeeMachine()
    cm.refill_water(1000)
    cm.refill_coffee(750)
    cm.refill_milk(300)
    cm.prepare_coffee("espresso")
    print(cm)
    cm.prepare_coffee("americano")
    print(cm)
    cm.prepare_coffee("cappuccino")
    print(cm)
